#httpd4t  超快地添加测试用的web api接口    


###需求简介     
做安卓客户端的时候，用http协议跟服务端通信，传一些字符串。定好了接口，在队友的服务端程序写好之前，自己写个假的服务端测试用    

###httpd4t.py    
基于python3的标准模块里的http.server写的，项目的核心代码都在这。我不知道这算不算框架。太小了。。。    
    
    
###urls.py    
在这里编写各url对应的方法，运行httpd4t的时候，这里的方法会被加载进去。    
+ 用装饰器描述method和uri   
+ ctx参数是http.server.BaseHTTPRequestHandler的子类（httpd4t.RequestHandler）的上下文（不要用它的wfile。用return来代替wfile）
+ return的str会作为http响应的内容    
+ return None会响应500报文    


###dev.py client.py    
+ 这两个文件都是提高开发效率的小工具，不是项目需求的内容  
+ dev.py可以在8888端口启动httpd4t.py,并监测当前路径下（包括子路径）文件变化，如果有py文件发生变化，就重启httpd4t.py。（注意：这个文件的代码是python2的，应为需要第三方模块watchdog，只装了python2的watchdog）    
+ cilent.py可以发出简单的http POST请求     
 

目前只做了POST, GET方法，静态URL地址, 返回可以有200,404,500    
